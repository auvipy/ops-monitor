# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Credential',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=254)),
                ('credential_type', models.CharField(max_length=254)),
                ('password', models.CharField(max_length=254)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('public_key', models.CharField(max_length=254)),
                ('private_key', models.CharField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hostname', models.CharField(max_length=128)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='HostGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=254)),
                ('description', models.TextField()),
                ('inventory', models.CharField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job_name', models.CharField(max_length=54)),
                ('playbook_file', models.FileField(upload_to=b'')),
                ('use_vault', models.BooleanField()),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Playbook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('playbook_name', models.CharField(max_length=254)),
                ('vault_password', models.CharField(max_length=128)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('credential', models.OneToOneField(to='tower.Credential')),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('job', models.ForeignKey(to='tower.Job')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='host',
            name='host_playbook',
            field=models.ForeignKey(to='tower.Playbook'),
        ),
        migrations.AddField(
            model_name='host',
            name='hostgroup',
            field=models.ForeignKey(to='tower.HostGroup'),
        ),
    ]
