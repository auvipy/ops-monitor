from django.db import models
from django.contrib.auth.models import User

# models goes here.
#enum: ['ssh', 'vault', 'git']

#enum: ['Completed', 'Failed', 'Running', 'Queued']


class Credential(models.Model):
	name = models.CharField(max_length=254)
	credential_type = models.CharField(max_length=254)
	password = models.CharField(max_length=254)
	created_at = models.DateTimeField(auto_now=True)
	public_key = models.CharField(max_length=254)
	private_key = models.CharField(max_length=254)

	def __unicode__(self):
		return self.name


class Playbook(models.Model):
	playbook_name = models.CharField(max_length=254)
	credential = models.OneToOneField(Credential)
	location = models.FileField()
	vault_password = models.CharField(max_length=128)
	created_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.playbook_name


class HostGroup(models.Model):
	name = models.CharField(max_length=254)
	description = models.TextField()
	inventory = models.CharField(max_length=254)

	def __unicode__(self):
		return self.name


class Host(models.Model):
	hostname = models.CharField(max_length=128)
	hostgroup = models.ForeignKey(HostGroup)
	host_playbook = models.ForeignKey(Playbook)
	created_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.host_playbook


class Job(models.Model):
	job_name = models.CharField(max_length=54)
	playbook_file = models.FileField()
	use_vault = models.BooleanField()
	created_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return job_name


class Task(models.Model):
	created_at = models.DateTimeField(auto_now=True)
	job = models.ForeignKey(Job)

	def __unicode__(self):
		return self.created_at



class User(models.Model):
	user = models.ForeignKey(User)


	def __unicode__(self):
		return self.user



